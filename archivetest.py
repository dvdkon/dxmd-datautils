#!/usr/bin/env python3
import sys
import structs

path = sys.argv[1]
a = structs.ARCH.parse_file(path)
#print(a)
print("-- Containing files:")
for f in a.metadata.containing_files:
    print(f.str)
print("-- Files:")
for f in a.metadata.files:
    print(f.hash.hex(), f.name.str)
    for p in f.parts:
        print(f"    cf={p.containing_file} off={p.offset} len={p.data_len} unk1={p.unk1}")
