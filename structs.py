from construct import *

# Inspired by https://zenhax.com/viewtopic.php?t=3193

# Filename list at the end (before BIN1.reslist)
EndFileNameList = Struct(
    filenames_len = Int32ul,
    filenames = RepeatUntil(obj_.index == this.filenames_len - 1, Struct(
        unk1 = Int32ul, # Same for all (?),
        index = Int16ul,
        unk2 = Int16ul,
        unk3 = Int64ul,
        name_len = Int32ul,
        name = Int32ul,
        unk4 = Const(b"\x00"*6),
    ))
)

def ResourceListItem(offset_base):
    return Struct(
        offset = Int64ul, # Offset in BIN1 + something, not in file
        length1 = Int32ul,
        length2 = Int32ul,
        # length1 often == length2, sometimes the MSB is different on length1.
        # TODO: more data samples
        data = If(this.offset != 0xFFFFFFFFFFFFFFFF,
                  Pointer(lambda obj: offset_base(obj._) + obj.offset, Struct(
                      length = Int32ul,
                      data = Aligned(4, Bytes(this.length))))),
    )

def ResourceList(offset_base):
    return Struct(
        res_len = Int32ul,
        res_metadata = Array(this.res_len,
                             ResourceListItem(lambda obj: offset_base(obj._._))),
    )

# Often prepended by 0x18, which is the header length and some other data,
# a small 1-item ResourceList for "all\0"
# 0300 0040 0400 0000 0400 0000 616c 6c00 1800 0000
RLIB = Struct(
    magic = Const(b"BILR"), # "RLIB" -- ResourceLibrary
    unk1 = Const(b"\x00"*8),
    unk2 = Int32ul,
    unk3 = Int64ul,
    res_list = ResourceList(0),
)

BIN1 = Struct(
    magic = Const(b"BIN1"),
    unk1 = Int32ul,
    reslist_offset = Int32ub, # XXX: Big endian? Why?
    # RLIB offsets (and maybe others) reference this position
    offset_base = Tell,
    unk3 = Int32ul,
    reslist = Pointer(this.offset_base + this.reslist_offset + 4, Struct(
        unk2 = Int32ul,
        unk3 = Int32ul,
        ress_len = Int32ul,
        ress = Array(this.ress_len, Struct(
            offset = Int32ul,
            res = Pointer(this._._.offset_base + 4 + this.offset,
                              ResourceListItem(this._._.offset_base)),
        )),
    ))
#    reslists = RepeatUntil(
#        obj_.offset == 0xFFFFFFFFFFFFFFFF,
#        Struct(
#            offset = Int64ul,
#            item_count1 = Int32ul,
#            item_count1 = Int32ul,
#            # item_count1 = item_count2 (3 samples) and decrease
#            # also both = res_list.res_len
#            res_list = If(this.offset != 0xFFFFFFFFFFFFFFFF,
#                          Pointer(this._.offset_base + this.offset,
    #                                  ResourceList(this._.offset_base)))
#        )),
)

Header = EmbeddedSwitch(
    Struct(
        pos = Tell,
        entries_len = Int32ul,
        unk1 = Int32ul,
    ),
    # I don't understand what this does and there are only two options in the
    # game's files. TODO
    # Also, embedding doesn't work with 
    this.entries_len, {
        1: Struct(
            unk2 = Const(bytes([0x10,0,0,0, 0x14,0,0,0])),
            unk3 = Int32ul,
            path = CString("utf8"),
        ),
        2: Struct(
            unk2 = Int32ul,
            unk3 = Const(bytes([0x14,0,0,0, 0x1c,0,0,0, 0,0,0,0x1])),
            path1_len = Int32ul,
            path1 = CString("utf8"),
            path2 = CString("utf8"),
        ),
    }
)

# TODO: Allow for big-endinan version
HLIB = Struct(
    magic = Const(b"BILH"), # "HLIB" -- "HeaderLibrary"
    header_len = Int32ul,
    unk1 = Int32ul,
    length = Int32ul,
    unk2 = Const(bytes([0xFF]*8)),
    header = If(this.header_len > 0, Header),
    bin1 = BIN1,
)

# Null-byte terminated Pascal string
PStringNBT = Struct(
    str = PascalString(Int32ul, "utf8"),
    null_byte = Const(b"\x00"),
)

ArchiveFilePart = Struct(
    # Each part can be saved in a different file. Look at
    # ARCH.metadata.containing_files[containing_file] to see the filename
    containing_file = Int32ul,
    unk1 = Int64ul,
    offset = Int64ul,
    data_len = Int64ul
)

ArchiveFile = Struct(
    # Luigi A.'s script says this should be a timestamp, but I don't
    # know how to get the time out of it
    file_stamp = Int64ul,
    hash = Bytes(16),
    name = PStringNBT,
    parts_len = Int32ul,
    parts = ArchiveFilePart[this.parts_len],
)

# Based on aluigi.org/bms/deus_ex_mankind_divided.bms
ARCH = Struct(
    magic = Const(b"ARCH"),
    unk1 = Int32ul,
    files_len = Int32ul,
    containing_files_len = Int32ul,
    metadata_offset = Int64ul,
    metadata = Pointer(this.metadata_offset, Struct(
        containing_files = PStringNBT[this._.containing_files_len],
        files = ArchiveFile[this._.files_len],
    )),
)
