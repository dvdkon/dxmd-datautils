#!/usr/bin/env python3
import sys
import structs

path = sys.argv[1]
h = structs.HLIB.parse_file(path)
#print(h)
for res in h.bin1.reslist.ress:
    print(f"\nRES @{res.offset}/{res.res.offset}")
    if res.res.data is not None:
        print(f"\t{res.res.data.data}")
